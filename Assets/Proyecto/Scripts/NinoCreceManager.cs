﻿using UnityEngine;
using System.Collections;

public class NinoCreceManager : MonoBehaviour 
{
  
  public Transform ninoAnimado;
  public Transform panelNinoCrece;
  public Renderer ninoColor;
  public UnityEngine.UI.Text edadTxt;
  public UnityEngine.UI.Text porcentajeTxt;
  public Transform mascara;
  public float[] porcentajeArray;
  public MainManager mainManager;
  public Transform[] panelesSimilares;

  private int lastVenenoIndex;

  float edad = 3;
  float porcentaje = .50f;

  Color[] componentesColor = {Color.cyan,Color.yellow, Color.red,Color.green};



	void Start () 
  {
    ninoAnimado.GetComponent<Animation>().Stop();
    porcentajeArray = new float[4];
    //forzar el llenado de valores
    //mover con datos de FB
    edad = 3;
    edadTxt.text = edad + " años";
    LlenaArrayVenenos();
    EscalaMascara();
    CambiaContenido(0);
	}
	

	void Update () 
  {
	
	}

  public void MueveSlide(UnityEngine.UI.Slider barra)
  {
    ninoAnimado.GetComponent<Animation>()["NinoCrece"].time = (barra.value - barra.minValue) /barra.maxValue * 40f/12f;
    ninoAnimado.GetComponent<Animation>()["NinoCrece"].speed = 0.0f;
    ninoAnimado.GetComponent<Animation>().Play();
    edadTxt.text = barra.value.ToString() + " años";
    edad = barra.value;
    LlenaArrayVenenos();
    CambiaContenido(lastVenenoIndex);
    EscalaMascara();
  }

  public void CambiaContenido(int index)
  {
    lastVenenoIndex = index;
    ninoColor.material.color = componentesColor[ index];
    SetPorcentaje(porcentajeArray[index]);
    Debug.Log("porcentaje nuevo : "+porcentajeArray[index]);
    
    porcentajeTxt.text = "Este producto aporta el "+(porcentajeArray[index]*100f).ToString("0.0")
      +"% de tu dieta diaria de";
    if(index==0)
      porcentajeTxt.text += " azúcares";
    if(index==1)
      porcentajeTxt.text += " sodio";
    if(index==2)
      porcentajeTxt.text += " calorías";
    if(index==3)
      porcentajeTxt.text += " grasas";

    LlenaArrayVenenos();
    EscalaMascara();
  }

  int DaylyQuantity()
  {
    return 0;
  }

  public void EscalaMascara()
  {
    float m = (edad + 28.48f) / -0.787f;
    float b = (edad + 26.183f) / 0.7083f;

    Debug.Log("m :" + m + " , b: " + b);
    float escala = (porcentaje * m) + b;
    Vector3 nuevaEscala = mascara.localScale;
    nuevaEscala.y = escala * 0.95f;
    mascara.localScale = nuevaEscala;
  }
  public void SetPorcentaje(float por)
  {
    porcentaje = por;
  }
  public void LlenaArrayVenenos()
  {
    //porcentajeArray=
    //true para hombre, false para mujer
    porcentajeArray[0] = mainManager.GetCurrentProductPercentage((int)edad, true, MainManager.ETipoVeneno.Azucar);
    porcentajeArray[1] = mainManager.GetCurrentProductPercentage((int)edad, true, MainManager.ETipoVeneno.Sodio);
    porcentajeArray[2] = mainManager.GetCurrentProductPercentage((int)edad, true, MainManager.ETipoVeneno.Energia);
    porcentajeArray[3] = mainManager.GetCurrentProductPercentage((int)edad, true, MainManager.ETipoVeneno.Grasas);
  }

  public void OpenSimilares()
  {
    panelNinoCrece.gameObject.SetActive(false);
    ninoAnimado.gameObject.SetActive(false);

      switch (mainManager.batCodeActual)
      {
      case "7501008042953": //zucaras
        panelesSimilares[0].gameObject.SetActive(true);
        break;
      case "7501000120253": //panbimbo
        panelesSimilares[1].gameObject.SetActive(true);
        break;
      case "7501000639021": //galletas
        panelesSimilares[2].gameObject.SetActive(true);
        break;
      case "7501040092350": //yogurt
        panelesSimilares[3].gameObject.SetActive(true);
        break;
      case "7501055300075": //cocacola
      case "7501055349401":
        panelesSimilares[4].gameObject.SetActive(true);
        break;
      default:

        break;
      }
  }

}
