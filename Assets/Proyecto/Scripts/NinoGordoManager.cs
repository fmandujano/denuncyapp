﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine.UI;
using Facebook.Unity;
public class NinoGordoManager : MonoBehaviour 
{

  public string[] complicaciones;
  public string[] tituloComp;

  public string[] DatosSabiasQue;
  public Transform compObj;
  public UnityEngine.UI.Text textoEncabezado;
  public UnityEngine.UI.Text textoComplicacion;
  public Text textoSabiasQue;
  public float showSpeed;

  float lerpTime;
  float iniTime;
  int edoAnimando=0;
  Vector3 iniPos;
  Vector3 endPos;
  Vector3 endSca;



	void Start () 
  {
    endPos = compObj.transform.position;

    if (!FB.IsInitialized) {
        // Initialize the Facebook SDK
        FB.Init(InitCallback, OnHideUnity);
    } else {
        // Already initialized, signal an app activation App Event
        FB.ActivateApp();
    }

    
	}

    private void InitCallback ()
  {
      if (FB.IsInitialized) {
          // Signal an app activation App Event
          FB.ActivateApp();
          // Continue with Facebook SDK
          // ...
      } else {
          Debug.Log("Failed to Initialize the Facebook SDK");
      }

      //subir como evento de Facebook

      var tutParams = new Dictionary<string, object>();
      tutParams["masinfo"] = 1;

      FB.LogAppEvent ( AppEventName.Searched, parameters: tutParams);
  }

  private void OnHideUnity (bool isGameShown)
  {
      if (!isGameShown) {
          // Pause the game - we will need to hide
          Time.timeScale = 0;
      } else {
          // Resume the game - we're getting focus again
          Time.timeScale = 1;
      }
  }
	
	void Update () 
  {
    if (edoAnimando == 1) //Mostrando
    {
      lerpTime = (Time.time - iniTime) * showSpeed;
      compObj.transform.position = Vector3.Slerp(iniPos,endPos,lerpTime);
      compObj.transform.localScale =  Vector3.Slerp(Vector3.zero,new Vector3(1f,1f,1f),lerpTime);
      //Debug.Log(Mathf.Abs(compObj.transform.position.x- endPos.x)<0.1f);


      if (Mathf.Abs(compObj.transform.position.x- endPos.x)<0.1f && Mathf.Abs(compObj.transform.position.y- endPos.y)<0.1f && Mathf.Abs(compObj.transform.position.z- endPos.z)<0.1f)
      {
        edoAnimando = 0;
        Debug.Log("termina anim");
      }
    }
    else if (edoAnimando == 2) //Ocultando
    {
      lerpTime = (Time.time - iniTime) * showSpeed;
      //compObj.transform.position = Vector3.Slerp(endPos,iniPos,lerpTime);
      compObj.transform.localScale =  Vector3.Slerp(new Vector3(1f,1f,1f),Vector3.zero,lerpTime);
      //Debug.Log(Mathf.Abs(compObj.transform.position.x- endPos.x)<0.1f);


      if (Mathf.Abs(compObj.transform.position.x- endPos.x)>0.9f && Mathf.Abs(compObj.transform.position.y- endPos.y)>0.9f && Mathf.Abs(compObj.transform.position.z- endPos.z)>0.9f)
      {
        edoAnimando = 0;
        Debug.Log("termina anim");
      }
    }
	}

  public void ShowComplicacion(Transform colider)
  {
    int index = int.Parse(colider.gameObject.name);

    iniPos = colider.transform.position;
    textoEncabezado.text = tituloComp[index];
    textoComplicacion.text = complicaciones[index].Replace("•", "\n•")    ;
    Debug.Log(iniPos);
    iniTime = Time.time;
    edoAnimando = 1;
  }

  public void EscalaZero(Transform trans)
  {
    trans.localScale = Vector3.zero;
  }

  public void EscalaUno(Transform trans)
  {
    trans.localScale = new Vector3(1f,1f,1f);
  }
  public void CierraComplicacion()
  {

    iniPos = Vector3.zero;
    iniTime = Time.time;
    edoAnimando = 2;
  }

  int idx;
  public void DesplegarDatoInteresante()
  {
    idx = ++idx%DatosSabiasQue.Length;
    Debug.Log(idx);
    //int idx = Random.Range(0, DatosSabiasQue.Length);
    textoSabiasQue.text = DatosSabiasQue[idx];

    //subir como evento de Facebook

      var tutParams = new Dictionary<string, object>();
      tutParams["datointeresante"] = idx;

      FB.LogAppEvent ( AppEventName.Searched, parameters: tutParams);
  }

  public void CargarMenu()
  {
    SceneManager.LoadScene("MainMenu");
  }

  public void OnPanelNinoGordo()
  {
    //subir barcode como evento de Facebook

      var tutParams = new Dictionary<string, object>();
      tutParams["obesidadinf"] = true;

      FB.LogAppEvent ( AppEventName.Searched, parameters: tutParams);
  }

}
