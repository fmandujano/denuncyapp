﻿using UnityEngine;
using System.Collections;

public class ProductSelector : MonoBehaviour 
{
	//referencia al objeto en escena
	public Transform producto3D;

	//para el demo, cada uno de los posibles modelos
	public Transform cocacola;
	public Transform panbimbo;
	public Transform galletas;
	public Transform zucaritas;
	public Transform yogurt;

	public void Set3dModel(string barcode)
	{
		//destruir todos los hijos de 3dmodel
		foreach(Transform t in producto3D.transform)
		{
			Destroy(t.gameObject);
		}

		Transform hijo = producto3D;
		switch(barcode)
		{
			case "7501008042953": //zucaras
				hijo = Instantiate(zucaritas, Vector3.zero, Quaternion.identity) as Transform;
			break;
			case "7501000120253": //panbimbo
			hijo = Instantiate(panbimbo, Vector3.zero, Quaternion.identity) as Transform;
			break;
			case "7501000639021": //galletas
			hijo = Instantiate(galletas, Vector3.zero, Quaternion.identity) as Transform;
			break;
			case "7501040092350": //yogurt
			hijo = Instantiate(yogurt, Vector3.zero, Quaternion.identity) as Transform;
			break;
			case "7501055300075": //cocacola
			case "7501055349401":
				hijo = Instantiate(cocacola, Vector3.zero, Quaternion.identity) as Transform;

			break;
			default:
				hijo = Instantiate(cocacola, Vector3.zero, Quaternion.identity) as Transform;
			break;

		}
		hijo.SetParent(producto3D, false);
	}
}
