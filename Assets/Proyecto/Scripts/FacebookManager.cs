﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;

public class FacebookManager : MonoBehaviour {

	void Awake ()
	{
		if (FB.IsInitialized) {
			FB.ActivateApp();
		} else {
			//Handle FB.Init
			FB.Init( () => {
				FB.ActivateApp();
			});
		}
	}

	// Unity will call OnApplicationPause(false) when an app is resumed
	// from the background
	void OnApplicationPause (bool pauseStatus)
	{
		// Check the pauseStatus to see if we are in the foreground
		// or background
		if (!pauseStatus) {
			//app resume
			if (FB.IsInitialized) {
				FB.ActivateApp();
			} else {
				//Handle FB.Init
				FB.Init( () => {
					FB.ActivateApp();
				});
			}
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void test(){
		Dictionary<string, object> softPurchaseParameters = new Dictionary<string, object>();
		softPurchaseParameters["mygame_purchased_item"] = "Un dato";
		FB.LogAppEvent(
			Facebook.Unity.AppEventName.SpentCredits,
			100.0f,
			softPurchaseParameters
		);
	}
}
