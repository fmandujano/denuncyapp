﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.IO;
using UnityEngine.SceneManagement;
using Facebook.Unity;
using System;
public class MainManager : MonoBehaviour 
{
    SQLiteDB db;
    MemoryStream memorystream;

    public string DebugCodigoProducto = "7501000639021";

    //Elementos de escena
    public Transform PanelPrincipal;
    public Transform PanelProducto;

      public Transform SemaforoAzucar;
      public Transform SemaforoEnergia;
      public Transform SemaforoGrasas;
      public Transform SemaforoSodio;
      public Text ProductData;
      public Text ProductWeight;

    public Transform PanelMasInfo;
    public string batCodeActual;

    private bool entero; // si se muestra el calculo por porcion o entero
    private float actualPorciones; //num de prociones que contiene el paquete
    private float actualEnergia;
    private float actualAzucar;
    private float actualGrasas; //suma de las grasas
    private float actualSodio;
    bool banderaComparacion = false;

  public Transform panelNino;
  public Transform panelDosNinos;
  public Transform dosNinosCrece;
  public Transform ninoCrece;
  
	void Start () 
  {
    db = new SQLiteDB();
    try
    {
      //db.Open(Application.streamingAssetsPath + "/data.sqlite");
      StartCoroutine(loadDatabase());
    } 
    catch (System.Exception e)
    {
    Debug.Log("error "+e);
    }

    if (!FB.IsInitialized) {
        // Initialize the Facebook SDK
        FB.Init(InitCallback, OnHideUnity);
    } else {
        // Already initialized, signal an app activation App Event
        FB.ActivateApp();
    }
    
	}

  private void InitCallback ()
  {
      if (FB.IsInitialized) {
          // Signal an app activation App Event
          FB.ActivateApp();
          // Continue with Facebook SDK
          // ...
      } else {
          Debug.Log("Failed to Initialize the Facebook SDK");
      }
  }

  private void OnHideUnity (bool isGameShown)
  {
      if (!isGameShown) {
          // Pause the game - we will need to hide
          Time.timeScale = 0;
      } else {
          // Resume the game - we're getting focus again
          Time.timeScale = 1;
      }
  }

  IEnumerator loadDatabase()
  {
    byte[] bytes = null;	
    string dbfilename = "data.sqlite";

#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
		string dbpath = "file://" + Application.streamingAssetsPath + "/" + dbfilename; 
		WWW www = new WWW(dbpath);
		yield return www;
		bytes = www.bytes;
#elif UNITY_WEBPLAYER
		string dbpath = "StreamingAssets/" + dbfilename;							
		WWW www = new WWW(dbpath);
		yield return www;
		bytes = www.bytes;
#elif UNITY_IPHONE
   

		string dbpath = Application.dataPath + "/Raw/" + dbfilename;		
    WWW www = new WWW(dbpath);
		try{	
			using ( FileStream fs = new FileStream(dbpath, FileMode.Open, FileAccess.Read, FileShare.Read) )
      {
				bytes = new byte[fs.Length];
				fs.Read(bytes,0,(int)fs.Length);
			}			
		} catch (Exception e){
    Debug.Log 	("\nTest Fail with Exception " + e.ToString());
  
		}
    yield return www;
#elif UNITY_ANDROID
		string dbpath = Application.streamingAssetsPath + "/" + dbfilename;
		WWW www = new WWW(dbpath);
		yield return www;
		bytes = www.bytes;
#endif

    if ( bytes != null )
		{
			try
			{	
				MemoryStream memStream = new MemoryStream();	
				memStream.Write(bytes,0,bytes.Length);
				db.OpenStream("stream2",memStream); 
        Debug.Log("db abierta");
				//mp.db.Close();
			}
			catch (System.Exception e)
			{
				Debug.Log("loadDatabase error with Exception " + e.ToString());
			}
		}
  }

	void Update () 
  {
	
	}

  //abrir escaner
  public void ScanQR()
  {
    #if UNITY_EDITOR 
    testQR();
    #else
  
    // Initialize EasyCodeScanner
    EasyCodeScanner.Initialize();

    //Register on Actions
    EasyCodeScanner.OnScannerMessage += onScannerMessage;
    EasyCodeScanner.OnScannerEvent += onScannerEvent;
    EasyCodeScanner.OnDecoderMessage += onDecoderMessage;
    EasyCodeScanner.launchScanner( true, "", -1, true);
    #endif
  }

  public void testQR()
  {
    DisplayProduct(DebugCodigoProducto);
  }

  void onScannerMessage(string data)
  {
    // Debug.Log("EasyCodeScannerExample - onScannerMessage data=:"+data);
    Debug.Log(data);

    DisplayProduct(data);
    //Just to show case : get the image and display it on a Plane
    //Texture2D tex = EasyCodeScanner.getScannerImage(200, 200);
    //PlaneRender.material.mainTexture = tex;

    //Just to show case : decode a texture/image - refer to code list
    //EasyCodeScanner.decodeImage(-1, tex);
  }

  void onDecoderMessage(string data)
  {
    Debug.Log(data);
    DisplayProduct(data);
  }

  void onScannerEvent(string eventStr)
  {
    Debug.Log("EasyCodeScannerExample - onScannerEvent:"+eventStr);
    DisplayProduct(eventStr);
  }


  void DisplayProduct(string barcode)
  {
    batCodeActual = barcode;
    string productdata = "";
    SQLiteQuery query = new SQLiteQuery( db, "SELECT * FROM producto WHERE id="+barcode);
    PanelPrincipal.gameObject.SetActive(false);
    PanelProducto.gameObject.SetActive(true);

    ProductData.text = "";
    ProductWeight.text = "";

			while(query.Step())
			{
				productdata += query.GetString("marca") + " " + query.GetString("nombre");
        transform.GetComponent<ProductSelector>().Set3dModel(barcode);
        ProductWeight.text = query.GetDouble("neto") + " " + query.GetString("unidad_neto");

        actualPorciones = (float)query.GetDouble("num_porciones");
        actualEnergia = query.GetInteger("energia_cont");
        actualAzucar = (float)query.GetDouble("azucar_cont");
        actualGrasas = (float)query.GetDouble("grasa_cont") + (float)query.GetDouble("grasasat_cont");
        actualSodio = (float)query.GetDouble("sodio_cont");
        //determinar cuales iconos deben mostrarse
        mostrarSemaforo();
			}
			//si no hubo registros
			if(productdata=="")
			{
				productdata = "No se encontró el producto";
			}
      ProductData.text = productdata;
			query.Release();

      //subir barcode como evento de Facebook

      var tutParams = new Dictionary<string, object>();
      tutParams["barcode"] = barcode;

      FB.LogAppEvent ( AppEventName.Searched, parameters: tutParams);
  }

  void OnDestroy() 
  {
    //Unregister
    EasyCodeScanner.OnScannerMessage -= onScannerMessage;
    EasyCodeScanner.OnScannerEvent -= onScannerEvent;
    EasyCodeScanner.OnDecoderMessage -= onDecoderMessage;
  }

  void mostrarSemaforo()
  {
    //comenzar apagando todos
    SemaforoAzucar.gameObject.SetActive(false);
    SemaforoEnergia.gameObject.SetActive(false);
    SemaforoGrasas.gameObject.SetActive(false);
    SemaforoSodio.gameObject.SetActive(false);
    //5% o menos es bajo, 20% o más es alto, todo lo demás es medio
    //arbitrariamente tomar como referencia mujeres de 15 años
    if( GetCurrentProductPercentage( 15, false, ETipoVeneno.Azucar) * actualPorciones  >= 0.2f  )
    {
      SemaforoAzucar.gameObject.SetActive(true);
    }
    if( GetCurrentProductPercentage( 15, false, ETipoVeneno.Energia) * actualPorciones >= 0.2f  )
    {
      SemaforoEnergia.gameObject.SetActive(true);
    }
    if( GetCurrentProductPercentage( 15, false, ETipoVeneno.Grasas) * actualPorciones >= 0.2f  )
    {
      SemaforoGrasas.gameObject.SetActive(true);
    }
    if( GetCurrentProductPercentage( 15, false, ETipoVeneno.Sodio) * actualPorciones >= 0.2f  )
    {
      SemaforoSodio.gameObject.SetActive(true);
    }
  }

  //Las categorías de los elementos de interés
  public enum ETipoVeneno
  {
    Azucar,
    Grasas,
    Energia,
    Sodio
  }

  //retorna la cantidad diario del producto escaneado
  //sex=true para hombre, false para mujer
  public float GetDailyQuantity(int age, bool sex,  ETipoVeneno element )
  {
    if(element == ETipoVeneno.Energia)
    {
      if(age > 18 )
        return sex? 2500:2000;
      else if( age <= 18 && age > 15)
        return sex? 2750:2200;
      else if( age <= 15 && age > 11)
        return sex? 2200:1850;
      else if( age <= 11 && age > 7)
        return sex? 1950:1750;
      else //de 7 a 0
      {
        return sex? 1700:1550;
      }
    }
    
    if(element == ETipoVeneno.Azucar)
    {
      if(age > 18 )
        return sex? 120:90;
      else if( age <= 18 && age > 15)
        return sex? 140:105;
      else if( age <= 15 && age > 11)
        return sex? 110:90;
      else if( age <= 11 && age > 7)
        return sex? 100:86;
      else //de 7 a 0
      {
        return sex? 85:75;
      }
    }
      if(element == ETipoVeneno.Grasas)
    {
      if(age > 18 )
        return sex? 125:90;
      else if( age <= 18 && age > 15)
        return sex? 130:105;
      else if( age <= 15 && age > 11)
        return sex? 110:95;
      else if( age <= 11 && age > 7)
        return sex? 100:90;
      else //de 7 a 0
      {
        return sex? 85:80;
      }
    }
    if(element == ETipoVeneno.Sodio)
    {
      //unidades de gramos
      if(age > 18 )
        return sex? 2.4f:2.4f;
      else if( age <= 18 && age > 15)
        return sex? 2.4f:2.4f;
      else if( age <= 15 && age > 11)
        return sex? 2.4f:2.4f;
      else if( age <= 11 && age > 7)
        return sex? 1.8f:1.8f;
      else //de 7 a 0
      {
        return sex? 1.1f:1.1f;
      }
    }
    else
    {
      return 0;
      //algo salio mal
      Debug.LogWarning("Tipo de veneno no especificado");
    }
  }

/*
Entrega un procentaje en decimal (entre 0 y 1) de la dieta diaria que provee el producto escaneado
*/
  public float GetCurrentProductPercentage(int age, bool sex,  ETipoVeneno element )
  {
    if(element == ETipoVeneno.Energia)
    {
      Debug.Log("calorias: "+actualEnergia+", cantidad diaria: "+GetDailyQuantity(age, sex,ETipoVeneno.Energia ));
      return actualEnergia / GetDailyQuantity(age, sex,ETipoVeneno.Energia );
    }
    if(element == ETipoVeneno.Grasas)
    {
      Debug.Log("grasas: "+actualGrasas+", cantidad diaria: "+GetDailyQuantity(age, sex,ETipoVeneno.Grasas ));
      return actualGrasas / GetDailyQuantity(age, sex,ETipoVeneno.Grasas );
    }
    if(element == ETipoVeneno.Sodio)
    {
      Debug.Log("sodio: "+actualSodio+", cantidad diaria: "+GetDailyQuantity(age, sex,ETipoVeneno.Sodio ));
      return actualSodio / GetDailyQuantity(age, sex,ETipoVeneno.Sodio );
    }
    if(element == ETipoVeneno.Azucar)
    {
      Debug.Log("azucar: "+actualAzucar+", cantidad diaria: "+GetDailyQuantity(age, sex,ETipoVeneno.Azucar ));
      return actualAzucar / GetDailyQuantity(age, sex,ETipoVeneno.Azucar);
    }
    else
    {
      return 0;
      //algo salio mal
      Debug.LogWarning("Tipo de veneno no especificado");
    }
  }

  //abre la escena
  public void OpenNextScene()
  {
    SceneManager.LoadScene("InfoNinoGordo");
  }
    public void SetBanderaComapra( bool compara)
    {
    banderaComparacion = compara;
    }

    public void VerificaSiCompara()
    {
        if (banderaComparacion)
        {
          panelDosNinos.gameObject.SetActive(true);
          dosNinosCrece.gameObject.SetActive(true);
        }
        else
        {
          panelNino.gameObject.SetActive(true);
          ninoCrece.gameObject.SetActive(true);
        }
    }
}
