﻿using UnityEngine;
using System.Collections;

public class PruebasBD : MonoBehaviour 
{
	SQLiteDB db;
	string datoslog;

	string IDcb = "";
	// Use this for initialization
	void Start () 
	{
		db = new SQLiteDB();
		
		try
		{
			db.Open(Application.streamingAssetsPath + "/data.sqlite");
		} 
		catch (System.Exception e)
		{
			Debug.Log("error "+e);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI()
	{
		GUI.Label(new Rect (10, 10,  Screen.width - 20, 50),"codigo de barras");
		IDcb =  GUI.TextField(new Rect (10, 70,  Screen.width - 20, 50), IDcb, 32);
		if( GUI.Button( new Rect(10, 130,200,50), "Buscar"))
		{
			datoslog = "";
			Debug.Log("Prueba de lectura de DB");
			SQLiteQuery query = new SQLiteQuery( db, "SELECT * FROM producto WHERE id="+IDcb);

			while(query.Step())
			{
				datoslog += query.GetString("marca") + " " + 
					query.GetString("nombre") + " ";

			}
			//si no hubo registros
			if(datoslog=="")
			{
				datoslog = " No se encontró el producto";
			}

			query.Release();
		}

		GUI.Label( new Rect(10, 190, Screen.width - 20, 500), datoslog);

	}

}
