﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EtiquetaNutricionalManager : MonoBehaviour 
{
	public GameObject infopanel;
	public List<GameObject> pantallas;
	// Use this for initialization
	public void showInfo(int index)
  {
		for(int i = 0; i < pantallas.Count; i++)
    {
			pantallas [i].SetActive (false);
		}
		pantallas [index].SetActive (true);
		infopanel.SetActive (true);
	}
	public void closeInfo()
  {
		infopanel.SetActive (false);
	}
}
